# Mattermost

## Introduction

This document provides an overview of the architecture for Mattermost and discusses a scenario for failover to another availability zone.

## Architecture

Mattermost webservice is the main component, supported by services from postgreSQL, sealed secrets and s3. Postgres data is replicated with help from pgBackRest, using the design pattern: https://access.crunchydata.com/documentation/postgres-operator/latest/architecture/disaster-recovery/.

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby)"]
    BackupVaultAZ2(BackupVault)
  end


  subgraph AZ1 ["AZ1 (active)"]

    subgraph Storage
      
      MattermostPluginsVolume(Plugins Volume)
      PostgresVolume(Postgres Volume 2x)
      PostgresBackupVolumeAZ1(Postgres BackupVolume)
    end     
    
    MattermostAZ1(Mattermost 1x)
    PostgresAZ1(Postgres 2x)
    MattermostAZ1 --SQL--> PostgresAZ1
    PostgresAZ1 --> PostgresVolume
    MattermostAZ1 -- objects--> BackupVaultAZ2
    MattermostAZ1 <-- plugins --- MattermostPluginsVolume
    
    PostgresAZ1 --> replication--> BackupVaultAZ2 & PostgresBackupVolumeAZ1
   
  end 
```

### Notes

|        |                                                                                                                                                                   |
| :----- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| HA     | Mattermost team edition runs just one pod, so this give some limitations on HA. The rollover by Mattermost is quite good, users will not quickly notice downtime. |
| Backup | Backups for the database are created by pgBackRest on volume and in S3. Backup of artefacts is out of scope for the Mattermost role.                              |

Ansible role for Mattermost creates sealed secrets for postgres and Mattermost teambot token in s3 bucket. The default installation will use the S3 in the same K8s cluster. For production is prefered to save the sealed secrets in the standby site as shown below. The reason is these secrets should be available to the standby site in case of failover.

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby)"]

    BackupVaultAZ2(BackupVault)
  end

  subgraph DeployerTool ["Platform-runner"]
    AnsibleRole(SonarQube Ansible role) -- sealedsecrets --> BackupVaultAZ2
  end 
```

## Failover

On failover of the datacenter, a restore in the other availability zone can be done quickly, because all the data items (pgBackRest, secrets, Mattermost artefacts) are available in the failover zone.

Promoting the standby is shown in the next diagram. The failover code will read the sealed secrets from the bucket, install these in the Mattermost namespace and proceeds with installing postgres as a standby, promote it and finally install Mattermost.
The standby site has the following preconditions for a failover:

- Postgres operator installed.
- Sealedsecrets controller installed and master keys synchronized with the other AZ.
- An s3-secret in the failover namespace with access credentials to read and write from bucket in S3 in its own AZ.
- PgBackRest backups available in s3 bucket.
- Secrets for Postgres users and teambot token available in s3 bucket.
- Mattermost artefacts available in s3 bucket.

Note that the plugin volume is not replicated. The plugins will only change on new installs of the Mattermost role. 

```mermaid
flowchart TD;

  subgraph AZ2 ["AZ2 (standby --> active)"]

    subgraph StorageAZ2 [Storage]
      MattermostPluginsVolume(Plugins Volume)
      BackupVaultVolumeAZ2(BackupVault Volume)
      PostgresBackupVolumeAZ2(Postgres BackupVolume)
      PostgresVolume(Postgres Volume 2x)   
    end     

    BackupVaultAZ2(BackupVault)
    BackupVaultAZ2--> BackupVaultVolumeAZ2
    
    MattermostAZ2(Mattermost 1x)
    PostgresAZ2(Postgres 2x)
    MattermostAZ2 --SQL--> PostgresAZ2
    PostgresAZ2 --> PostgresVolume
    MattermostAZ2 -- objects--> BackupVaultAZ2
    MattermostAZ2 <-- plugins --- MattermostPluginsVolume
    PostgresAZ2 --> replication--> BackupVaultAZ2 & PostgresBackupVolumeAZ2
 
  end

  subgraph AZ1 ["AZ1 (down)"]
    down
  end 
```
